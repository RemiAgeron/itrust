import React from 'react'
import { Button, Container, Divider, Grid, Header } from 'semantic-ui-react'
import EventsViewer from '../component/EventsViewer'
import EventModal from '../component/EventModal';
import { notify } from '../constant';

const Index = () => {
  const [events, setEvents] = React.useState<[Date, string][]>([
    [new Date('10/07/2010'), 'Event number 150.'],
    [new Date('03/05/2022'), 'Event number 10.'],
    [new Date('06/21/2015'), 'Event number 15.'],
    [new Date('06/18/2021'), 'Event number 1.'],
    [new Date('10/12/2020'), 'Event number 50.'],
    [new Date('1/04/2018'), 'Event number 42.'],
    [new Date(), 'Event number 56.']
  ])

  const handleRemoveEvent = (event: [Date, string]) => {
    setEvents(events.filter(currentEvent => currentEvent[0] !== event[0] || currentEvent[1] !== event[1]));
    notify('success', 'Event successfully removed');
  }

  const handleAddEvent = (event: [Date, string]) => {
    setEvents([...events, event]);
    notify('success', 'Event successfully added');
  }

  const handleUpdateEvent = (event: [Date, string], eventIndex: number) => {
    setEvents((prevEvents) => {
      const copy = new Array(...prevEvents);
      copy[eventIndex] = event;
      return copy;
    });
    notify('success', 'Event successfully updated');
  }

  const duplicateEvent = (event: [Date, string]): boolean => {
    return events.filter(currentEvent => currentEvent[0].getTime() === event[0].getTime() && currentEvent[1] === event[1]).length > 0
  }

  const [openModalEvent, setOpenModalEvent] = React.useState<boolean>(false);

  return (
    <Container>
      <EventModal openModalEvent={openModalEvent} setOpenModalEvent={setOpenModalEvent} handleAddEvent={handleAddEvent} handleUpdateEvent={handleUpdateEvent} duplicateEvent={duplicateEvent} />
      <Divider hidden />
      <Grid verticalAlign='middle' columns='equal'>
        <Grid.Column>
          <Header>Event Storage Viewer</Header>
        </Grid.Column>
        <Grid.Column textAlign='right' floated='right'>
          <Button color='red' onClick={() => setOpenModalEvent(true)}>New Event</Button>
        </Grid.Column>
      </Grid>
      <Divider section />
      <EventsViewer events={events} handleRemoveEvent={handleRemoveEvent} handleAddEvent={handleAddEvent} handleUpdateEvent={handleUpdateEvent} duplicateEvent={duplicateEvent} />
    </Container>
  )
}

export default Index