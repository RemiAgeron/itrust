import toast from "react-hot-toast";

// NOTIFICATIONS
export let toastId: string;

export const notify = (option: "loading" | "success" | "error" | "blank", text?: string) => {
  switch (option) {
    case "loading":
      toastId = toast.loading(text || "Loading", {
        id: toastId,
      });
      break;
    case "success":
      toastId = toast.success(text || "Success !", {
        id: toastId,
      });
      break;
    case "error":
      toastId = toast.error(text || "Error", {
        id: toastId,
      });
      break;
    default:
      toastId = toast(text || "Need more information", {
        id: toastId,
      });
      break;
  }
};