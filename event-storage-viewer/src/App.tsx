import React from 'react';
import { Toaster } from 'react-hot-toast';
import Index from './page/Index';

function App() {
  return (
    <>
      <Toaster />
      <Index />
    </>
  );
}

export default App;
