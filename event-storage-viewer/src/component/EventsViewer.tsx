import React from 'react'
import { Table } from 'semantic-ui-react'
import EventViewerRow from './EventViewerRow';

const EventsViewer = ({ events, handleAddEvent, handleRemoveEvent, handleUpdateEvent, duplicateEvent }: { events: [Date, string][], handleAddEvent: Function, handleRemoveEvent: Function, handleUpdateEvent: Function, duplicateEvent: Function }) => {

  return (
    <>
      <Table>
        <Table.Header>
          <Table.Row>
            {['DATE', 'DESCRIPTION', ''].map(header => <Table.HeaderCell key={header} textAlign='center'>{header}</Table.HeaderCell>)}
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {events.map((event, index) => <EventViewerRow key={index} event={event} handleRemoveEvent={handleRemoveEvent} handleAddEvent={handleAddEvent} handleUpdateEvent={handleUpdateEvent} index={index} duplicateEvent={duplicateEvent} />)}
        </Table.Body>
      </Table>
    </>
  )
}

export default EventsViewer