import React from 'react'
import { Button, Modal, Table } from 'semantic-ui-react'
import EventModal from './EventModal';

const EventViewerRow = ({index, event, handleAddEvent, handleRemoveEvent, handleUpdateEvent, duplicateEvent}: {index: number, event: [Date, string], handleAddEvent: Function, handleRemoveEvent: Function, handleUpdateEvent: Function, duplicateEvent: Function}) => {
  const [openModalUpdate, setOpenModalUpdate] = React.useState<boolean>(false);
  const [openModalRemove, setOpenModalRemove] = React.useState<boolean>(false);

  return (
    <>
      <Modal size='small' open={openModalRemove} onClose={() => setOpenModalRemove(false)}>
        <Modal.Header>Are you sure you want to delete : {event[1]}</Modal.Header>
        <Modal.Actions>
          <Button positive onClick={() => {handleRemoveEvent(event); setOpenModalRemove(false)}}>Confirm</Button>
          <Button negative onClick={() => setOpenModalRemove(false)}>Cancel</Button>
        </Modal.Actions>
      </Modal>
      <EventModal openModalEvent={openModalUpdate} setOpenModalEvent={setOpenModalUpdate} handleAddEvent={handleAddEvent} handleUpdateEvent={handleUpdateEvent} currentEvent={event} index={index} duplicateEvent={duplicateEvent}/>
      <Table.Row>
        <Table.Cell textAlign='center'>{event[0].toLocaleDateString()}</Table.Cell>
        <Table.Cell textAlign='center'>{event[1]}</Table.Cell>
        <Table.Cell textAlign='center'>
          <Button icon='pencil' onClick={() => setOpenModalUpdate(true)} />
          <Button icon='trash' color='red' onClick={() => setOpenModalRemove(true)} />
        </Table.Cell>
      </Table.Row>
    </>
  )
}

export default EventViewerRow