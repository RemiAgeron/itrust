import React from 'react'
import { useForm } from 'react-hook-form';
import { Button, Divider, Form, Modal } from 'semantic-ui-react'
import { notify } from '../constant';

const EventModal = ({ openModalEvent, setOpenModalEvent, duplicateEvent, handleAddEvent, handleUpdateEvent, currentEvent, index }: { openModalEvent: boolean, setOpenModalEvent: Function, duplicateEvent: Function, handleAddEvent: Function, handleUpdateEvent: Function, currentEvent?: [Date, string], index?: number }) => {
  const { register, handleSubmit, reset } = useForm<{ date?: string, description?: string }>();
  const onSubmit = (data: { date?: string, description?: string }) => {
    try {

      if (data.date === undefined || data.date.trim() === '') {
        notify('error', 'Date is required');
        throw new Error;
      } else if (data.description === undefined || data.description.trim() === '') {
        notify('error', 'Description is required');
        throw new Error;
      }

      if (duplicateEvent([new Date(data.date), data.description])) {
        notify('error', 'Event already exist');
        throw new Error;
      }
      if (currentEvent !== undefined && index !== undefined) {
        handleUpdateEvent([new Date(data.date), data.description], index)
      } else {
        handleAddEvent([new Date(data.date), data.description]);
      }

      reset();
      setOpenModalEvent(false);

    } catch (error) { }

  }

  return (
    <Modal size='small' open={openModalEvent} onClose={() => setOpenModalEvent(false)}>
      <Modal.Header>Add a new event</Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group inline>
            <Form.Input label="Date :">
              <input type="date" defaultValue={currentEvent ? [currentEvent[0].getFullYear(), ('0' + (currentEvent[0].getMonth() + 1)).slice(-2), ('0' + currentEvent[0].getDate()).slice(-2)].join('-') : undefined} {...register('date')} />
            </Form.Input>
            <Form.Input label="Description :">
              <input type="text" defaultValue={currentEvent ? currentEvent[1] : undefined} {...register('description')} />
            </Form.Input>
          </Form.Group>
          <Divider />
          <Button positive type='submit'>Confirm</Button>
          <Button negative onClick={() => setOpenModalEvent(false)}>Cancel</Button>
        </Form>
      </Modal.Content>
    </Modal>
  )
}

export default EventModal